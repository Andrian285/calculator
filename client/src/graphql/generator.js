import gql from 'graphql-tag'

export function generateQuery(queries) {
    let combined = `query{`
    for (let query of queries) combined += `...` + /([A-Z])\w+/.exec(query)[0] + ` `
    combined += '}'
    for (let query of queries) combined += query
    return gql(combined)
}

export function generateQueryWithVariables(query) {
    const variablesTypesRegex = /\(.*\)/
    const typeRegex = /([A-Z])\w+\!?/g
    const lastVariableRegex = /\w+\:(?!.*\:)/

    const types = variablesTypesRegex.exec(query)[0]
    let combined = `query Query` + types + `{...` + /([A-Z])\w+/.exec(query)[0] + `}`
    const variables = types.replace(/\$/g, '').replace(typeRegex, (match, foundString, offset) => {
        let word = types.substring(0, offset).match(lastVariableRegex)[0]
        return '$' + word.substring(0, word.length - 1)
    })
    combined += query.replace(/\(.*\)/, variables)
    return gql(combined)
}
