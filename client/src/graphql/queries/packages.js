export default `
  fragment Packages on Query {
    packages {
      id,
      name,
      weight,
      measure,
      percentLoss,
      container {
        ... on Raw {id, name, __typename}
        ... on Product {id, name, __typename}
      },
      label {
        ... on Raw {id, name, __typename}
        ... on Product {id, name, __typename}
      },
      sticker {
        ... on Raw {id, name, __typename}
        ... on Product {id, name, __typename}
      }
    }
  }
`