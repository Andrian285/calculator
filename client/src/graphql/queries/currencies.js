export default `
  fragment Currencies on Query {
    currencies {
        id,
        name,
        sign
    }
  }
`