import {groups, group} from './productGroups'
import { products, product } from './products'
import {raw, rawById} from './raw'
import currencies from './currencies'
import cost from './cost'
import packages from './packages'

export const GET_GROUPS = groups
export const GET_GROUP = group
export const GET_RAW = raw
export const GET_RAW_BY_ID = rawById
export const GET_CURRENCIES = currencies
export const GET_PRODUCTS = products
export const GET_PRODUCT = product
export const GET_COST = cost
export const GET_PACKAGES = packages