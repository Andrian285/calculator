export default `
fragment Cost on Query {
  cost($input: CostInput!) {
    mainRawCost,
    additionalRawCost,
    salaryCost,
    prodCost,
    adminCost
  }
}
`