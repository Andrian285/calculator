export const products = `
fragment Products on Query {
  products {
    id,
    name,
    priceInclude,
    code,
    expCode,
    packages {id, name}
    __typename
  }
}
`

export const product = `
fragment Product on Query {
  product($id: Int!) {
    id,
    name,
    priceInclude,
    code,
    expCode,
    packages {id, name, cost, weight, measure},
    receipt {
      ... on Raw {isRaw, id, amount, name, componentId, price, currency {id, sign}, isMain},
      ... on Product {isRaw, id, amount, name, componentId, price, currency {id, sign}}
    }
  }
}
`