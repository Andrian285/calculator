export const raw = `
  fragment Raw on Query {
    raws {
        id,
        name,
        isMain,
        measure,
        price
        currency {
          name
          id
          sign
        },
        __typename
    }
  }
`

export const rawById = `
  fragment Raw on Query {
    raw($id: Int!) {
        id,
        name,
        isMain,
        measure,
        price
        currency {
          name
          id
          sign
        }
    }
  }
`