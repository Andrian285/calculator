export const groups = `
  fragment Groups on Query {
    groups {
      id,
      name,
      baseSalary,
      percentAddSalary,
      percentProfit
      percentProdCosts,
      percentAdminCosts,
      priceNumber
    }
  }
`

export const group = `
  fragment Group on Query {
    group($id: Int!) {
      id,
      name,
      baseSalary,
      percentAddSalary,
      percentProfit
      percentProdCosts,
      percentAdminCosts,
      priceNumber,
      products {
        id,
        name,
        priceInclude,
        code,
        expCode,
        packages {id, name}
      }
    }
  }
`