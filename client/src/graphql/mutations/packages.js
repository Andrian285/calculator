import gql from 'graphql-tag'

export const save = gql`
  mutation packageSave(
    $id: Int,
    $name: String!, 
    $weight: Float!,
    $measure: String!,
    $percentLoss: Float!,
    $containerId: Int,
    $containerRaw: Boolean,
    $labelId: Int,
    $labelRaw: Boolean,
    $stickerId: Int,
    $stickerRaw: Boolean,
  ) 
  {
    packageSave (
      id: $id,
      name: $name, 
      weight: $weight,
      measure: $measure,
      percentLoss: $percentLoss,
      containerId: $containerId,
      containerRaw: $containerRaw,
      labelId: $labelId,
      labelRaw: $labelRaw,
      stickerId: $stickerId,
      stickerRaw: $stickerRaw,
    )
  }
`

export const remove = gql`
  mutation packageDelete($id: Int!) {
    packageDelete(id: $id)
  }
`