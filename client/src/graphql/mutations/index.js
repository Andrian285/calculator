import { save as saveGroup, remove as removeGroup } from './productGroups'
import { save as saveRaw, remove as removeRaw } from './raw'
import { save as saveProduct, remove as removeProduct } from './products'
import { save as savePackage, remove as removePackage } from './packages'

export const SAVE_GROUP = saveGroup
export const REMOVE_GROUP = removeGroup
export const SAVE_RAW = saveRaw
export const REMOVE_RAW = removeRaw
export const SAVE_PRODUCT = saveProduct
export const REMOVE_PRODUCT = removeProduct
export const SAVE_PACKAGE = savePackage
export const REMOVE_PACKAGE = removePackage