import gql from 'graphql-tag'

export const save = gql`
  mutation rawSave(
    $id: Int,
    $name: String!, 
    $isMain: Boolean!, 
    $measure: String!, 
    $price: Float, 
    $isVAT: Boolean!,
    $currencyId: Int!
  ) 
  {
    rawSave (
      id: $id,
      name: $name, 
      isMain: $isMain, 
      measure: $measure, 
      price: $price, 
      isVAT: $isVAT,
      currencyId: $currencyId
    )
  }
`

export const remove = gql`
  mutation rawDelete($id: Int!) {
    rawDelete(id: $id)
  }
`