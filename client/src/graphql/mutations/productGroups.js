import gql from 'graphql-tag'

export const save = gql`
  mutation groupSave(
    $id: Int,
    $name: String!, 
    $baseSalary: Int!, 
    $percentAddSalary: Int!, 
    $percentProfit: Int!, 
    $percentProdCosts: Int!,
    $percentAdminCosts: Int!,
    $priceNumber: Int!
  ) 
  {
    groupSave (
      id: $id,
      name: $name, 
      baseSalary: $baseSalary, 
      percentAddSalary: $percentAddSalary, 
      percentProfit: $percentProfit, 
      percentProdCosts: $percentProdCosts,
      percentAdminCosts: $percentAdminCosts,
      priceNumber: $priceNumber
    )
  }
`

export const remove = gql`
  mutation groupDelete($id: Int!) {
    groupDelete(id: $id)
  }
`