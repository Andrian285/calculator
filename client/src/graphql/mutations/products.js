import gql from 'graphql-tag'

export const save = gql`
  mutation productSave(
    $id: Int,
    $name: String!, 
    $code: String!, 
    $expCode: String!, 
    $priceInclude: Boolean!, 
    $groupId: Int!,
    $receipt: [ComponentInput],
    $packages: [Int]
  ) 
  {
    productSave (
      id: $id,
      name: $name, 
      code: $code, 
      expCode: $expCode, 
      priceInclude: $priceInclude, 
      groupId: $groupId,
      receipt: $receipt,
      packages: $packages
    )
  }
`

export const remove = gql`
  mutation productDelete($id: Int!) {
    productDelete(id: $id)
  }
`