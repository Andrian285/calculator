import React, { Component } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { Provider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import Navbar from './components/Navbar'
import ProductGroups from './containers/ProductGroups'
import Products from './containers/Products'
import Product from './containers/Product'
import Raw from './containers/Raw'
import ErrorLogger from './components/ErrorLogger'
import Packages from './containers/Packages'

const alertOpts = {
    timeout: 5000,
    position: 'top center',
}

class MainRedirector extends Component {
    render() {
        return (
            <Redirect to='/product-groups' />
        )
    }
}

export default class App extends Component {
    render() {
        return (
            <Provider template={AlertTemplate} {...alertOpts}>
                <BrowserRouter>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <Navbar />
                        <div style={{ textAlign: 'center', width: '100%' }}>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
                                <Switch>
                                    <Route exact path='/' component={MainRedirector} />
                                    <Route exact path='/product-groups' component={ProductGroups} />
                                    <Route exact path='/product-groups/:id' component={Products} />
                                    <Route exact path='/product-groups/:groupId/:id' component={Product} />
                                    <Route exact path='/raw' component={Raw} />
                                    <Route exact path='/packages' component={Packages} />
                                    <Route render={() => <ErrorLogger error='Сторінки не знайдено' />} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </BrowserRouter >
            </Provider>
        )
    }
}