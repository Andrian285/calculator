import React, { Component } from 'react'
import CustomTable from '../components/CustomTable'
import config from '../utils/config'
import CustomModal from '../components/CustomModal'
import { withAlert } from 'react-alert'
import { withGraphQL, withImmutable } from '../hoc'
import { GET_RAW, GET_CURRENCIES, SAVE_RAW, REMOVE_RAW } from '../graphql'
import ErrorLogger from '../components/ErrorLogger'
import Confirm from '../components/Confirm'
import Loading from '../components/Loading'

const errors = config.errors

const displayHeads = [
    {
        label: 'Назва, міра',
        keys: [{ prop: 'name' }, { prop: 'measure' }],
        separator: ','
    },
    {
        label: 'Входить в рецептуру',
        keys: [{ prop: 'isMain', checkbox: true }],
    },
    {
        label: 'Ціна',
        keys: [{ prop: 'price' }, { prop: 'currency', dropdown: true, keyName: 'sign' }],
        numeric: true
    },
]

const heads = [
    { label: 'Назва', key: 'name' },
    { label: 'Міра', key: 'measure' },
    { label: 'Входить в рецептуру', key: 'isMain', checkbox: true },
    { label: 'Ціна', key: 'price', numeric: true },
    { label: 'Валюта', key: 'currencyId', dropdown: true },
    { label: 'з ПДВ', key: 'isVAT', checkbox: true },
]

const additionalOpts = {
    tableName: 'Сировина'
}

class Raw extends Component {
    state = {
        openModal: false,
        modalValues: {},
        loading: true,
        confirmRemoveOpen: false,
        removeElm: {},
        error: null
    }

    async componentDidMount() {
        await this.props.onQuery([{ query: GET_RAW }, { query: GET_CURRENCIES }])
        this.setState({ loading: false })
    }

    formatDropdowns(modalValues) {
        modalValues.currencyId = {
            current: modalValues.currency.id,
            items: this.props.copy(this.props.fetchedData.currencies)
        }
    }

    onEditModalOpen = (e, row) => {
        e.stopPropagation()
        let modalValues = this.props.copy(row)
        this.formatDropdowns(modalValues)
        this.setState({ modalValues, openModal: true })
    }

    onAddModalOpen = () => {
        let modalValues = {}
        if (this.props.fetchedData.currencies.length > 0) {
            modalValues.currencyId = {
                current: this.props.fetchedData.currencies[0].id,
                items: this.props.copy(this.props.fetchedData.currencies)
            }
        } else return this.props.alert.error('Для початку вам потрібно створити валюту')
        this.setState({ modalValues, openModal: true })
    }

    render() {
        if (this.state.error) return <ErrorLogger error={this.state.error} />
        if (this.state.loading) return <Loading />
        if (!this.props.fetchedData || !this.props.fetchedData.currencies || 
            !this.props.fetchedData.raws) return <ErrorLogger error='Щось пішло не так :(' />
        return (
            <div>
                <CustomModal
                    open={this.state.openModal}
                    handleClose={(data) => {
                        this.setState({ openModal: false, modalValues: {} })
                        if (data) this.props.onMutate({ 
                            mutation: SAVE_RAW,
                            variables: data
                        }, errors.invalidData, [{ query: GET_RAW }])
                    }}
                    keys={heads}
                    values={this.state.modalValues}
                />
                <CustomTable
                    heads={displayHeads}
                    data={this.props.fetchedData.raws}
                    onAdd={this.onAddModalOpen}
                    onEdit={this.onEditModalOpen}
                    onRemove={(element) => this.setState({ removeElm: element, confirmRemoveOpen: true })}
                    {...additionalOpts}
                />
                <Confirm
                    open={this.state.confirmRemoveOpen}
                    title={'Ви впевнені, що бажаєте видалити дану сировину?'}
                    details={'Назва сировини: ' + this.state.removeElm.name}
                    onResult={(statement) => {
                        this.setState({ confirmRemoveOpen: false })
                        if (statement) this.props.onMutate({
                            mutation: REMOVE_RAW,
                            variables: { id: this.state.removeElm.id }
                        }, errors.unpredictable, [{ query: GET_RAW }])
                    }}
                />
            </div>
        )
    }
}

export default withImmutable(withGraphQL(withAlert(Raw)))
