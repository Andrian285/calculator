import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import CustomTable from '../components/CustomTable'
import CostDataDialog from '../components/CostDataDialog'
import '../stylesheets/calendar.css'
import config from '../utils/config'
import CustomModal from '../components/CustomModal'
import { withAlert } from 'react-alert'
import BackButton from '../components/BackButton'
import { withGraphQL, withImmutable } from '../hoc'
import { GET_PRODUCT, GET_PRODUCTS, GET_RAW, GET_GROUP, SAVE_PRODUCT, GET_COST, GET_RAW_BY_ID, GET_PACKAGES } from '../graphql'
import Loading from '../components/Loading'
import ErrorLogger from '../components/ErrorLogger'
import Confirm from '../components/Confirm'
import TextField from '@material-ui/core/TextField'
import Switch from '@material-ui/core/Switch'
import moment from 'moment'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

const errors = config.errors

const displayHeads = [
    {
        label: 'Тип',
        keys: [{ prop: 'isRaw', dropdown: true, keyName: 'name' }],
    },
    {
        label: 'Назва',
        keys: [{ prop: 'name' }],
    },
    {
        label: 'Ціна',
        keys: [{ prop: 'price' }, { prop: 'currency', dropdown: true, keyName: 'sign' }],
        numeric: true
    },
    {
        label: 'Кількість (кг)',
        keys: [{ prop: 'amount' }],
        numeric: true
    },
]

const heads = [
    { label: 'Сировина', key: 'isRaw', dropdown: true },
    { label: 'Назва', key: 'componentId', dropdown: true },
    { label: 'Кількість (кг)', key: 'amount', numeric: true },
]

const isRawDropdownToCheckbox = (isRaw) => {
    return isRaw ? true : false
}

const isRawCheckboxToDropdown = (isRaw, isMain) => {
    return { name: isRaw ? 'Сировина (' + (isMain ? 'основна' : 'додаткова') + ')' : 'Продукт', id: isRaw ? 1 : 0 }
}

const swipeIds = (elm) => {
    const componentId = elm.componentId
    elm.componentId = elm.id
    elm.id = componentId
}

class Product extends Component {
    state = {
        product: null,
        modalValues: null,
        showCalendar: false,
        title: '',
        names: [],
        openModal: false,
        group: null,
        loading: true,
        confirmRemoveOpen: false,
        removeElm: {},
        selectedDate: moment(new Date()).format('YYYY-MM-DD'),
        testMode: false,
        percentRetail: 10,
        percentSalaryCharges: 22
    }

    async componentDidMount() {
        let res = await this.props.onQuery([{ query: GET_PRODUCT, variables: { id: this.props.match.params.id } },
        { query: GET_GROUP, variables: { id: this.props.match.params.groupId } }], false)
        if (res && res.product && res.group) {
            for (let elm of res.product.receipt) {
                elm.isRaw = isRawCheckboxToDropdown(elm.isRaw, elm.isMain)
                swipeIds(elm)
            }
            this.setState({ title: res.product.name + ': ' + res.group.name, product: res.product, group: res.group })
        }
        this.setState({ loading: false })
    }

    alreadyExists(id, isRaw) {
        for (let elm of this.state.product.receipt) {
            if (elm.isRaw && isRaw && elm.componentId === id) return true
            if (!elm.isRaw && !isRaw && elm.componentId === id) return true
        }
        return false
    }

    async getProducts(cb) {
        let res = await this.props.onQuery([{ query: GET_PRODUCTS }], false)
        if (res && res.products) {
            res.products = res.products.filter(elm => elm.id !== this.state.product.id && !this.alreadyExists(elm.id, false))
            let modalValues = this.state.modalValues
            if (modalValues && modalValues.componentId) {
                modalValues.componentId = {
                    current: res.products[0] ? res.products[0].id : '',
                    items: res.products
                }
                this.setState({ modalValues }, () => cb && cb())
            } else this.setState({ names: res.products }, () => cb && cb())
        }
    }

    async getRaw(cb) {
        let res = await this.props.onQuery([{ query: GET_RAW }], false)
        if (res && res.raws) {
            res.raws = res.raws.filter(elm => !this.alreadyExists(elm.id, true))
            let modalValues = this.state.modalValues
            if (modalValues && modalValues.componentId) {
                modalValues.componentId = {
                    current: res.raws[0] ? res.raws[0].id : '',
                    items: res.raws
                }
                this.setState({ modalValues }, () => cb && cb())
            } else this.setState({ names: res.raws }, () => cb && cb())
        }
    }

    formatDropdowns(modalValues) {
        modalValues.isRaw = {
            current: modalValues.isRaw && modalValues.isRaw.id ? modalValues.isRaw.id : 0,
            items: [{ name: 'Продукт', id: 0 }, { name: 'Сировина', id: 1 }]
        }
        modalValues.componentId = {
            current: modalValues.componentId || (this.state.names[0] && this.state.names[0].id) || '',
            items: this.props.copy(this.state.names)
        }
    }

    onEditModalOpen = (e, row) => {
        e.stopPropagation()
        let modalValues = this.props.copy(row)
        if (modalValues.isRaw.id) this.getRaw(() => {
            this.formatDropdowns(modalValues)
            this.setState({ modalValues, openModal: true })
        })
        else this.getProducts(() => {
            this.formatDropdowns(modalValues)
            this.setState({ modalValues, openModal: true })
        })
    }

    onAddModalOpen = () => {
        let modalValues = {}
        this.getProducts(() => {
            this.formatDropdowns(modalValues)
            this.setState({ modalValues, openModal: true })
        })
    }

    onModalClose = async (data) => {
        this.setState({ openModal: false, modalValues: {} })
        if (data) {
            let fields = this.props.copy(this.state.product)
            fields.groupId = this.state.group.id
            fields.packages = fields.packages.map(elm => elm.id)
            // if (!data.componentId) return this.props.alert.error('Всі поля повинні бути заповненими!')

            let exists = fields.receipt.find(elm => elm.id === data.id)
            let id = null
            if (exists) id = fields.receipt.indexOf(exists)
            let formattedData = {
                amount: data.amount,
                isRaw: isRawDropdownToCheckbox(data.isRaw),
                componentId: data.componentId
            }

            if (!this.state.testMode) {
                fields.receipt = fields.receipt.map(elm => {
                    return {
                        amount: elm.amount,
                        isRaw: isRawDropdownToCheckbox(elm.isRaw.id),
                        componentId: elm.componentId
                    }
                })
                if (id !== null) fields.receipt[id] = formattedData
                else fields.receipt.push(formattedData)
                let res = await this.props.onMutate({
                    mutation: SAVE_PRODUCT,
                    variables: fields
                }, errors.invalidData, [{ query: GET_PRODUCT, variables: { id: this.props.match.params.id } }])
                for (let elm of res.queryResponse.product.receipt) {
                    elm.isRaw = isRawCheckboxToDropdown(elm.isRaw, elm.isMain)
                    swipeIds(elm)
                }
                this.setState({ product: res.queryResponse.product })
            } else {
                if (data.isRaw) {
                    let res = await this.props.onQuery([{ query: GET_RAW_BY_ID, variables: { id: data.componentId } }], false)
                    data.name = res.raw.name
                    data.isVAT = res.raw.isVAT
                    data.price = res.raw.price
                    data.currency = res.raw.currency
                    data.isRaw = isRawCheckboxToDropdown(data.isRaw, res.raw.isMain)
                } else {
                    let res = await this.props.onQuery([{ query: GET_PRODUCT, variables: { id: data.componentId } }], false)
                    data.name = res.product.name
                    data.isRaw = isRawCheckboxToDropdown(data.isRaw)
                }
                data.id = Math.floor(Math.random() * Math.floor(1000))
                if (id !== null) fields.receipt[id] = data
                else fields.receipt.push(data)
                this.setState({ product: fields })
            }
        }
    }

    onRemove = async (statement) => {
        this.setState({ confirmRemoveOpen: false })
        if (statement) {
            let fields = this.props.copy(this.state.product)
            fields.groupId = this.state.group.id
            fields.packages = fields.packages.map(elm => elm.id)
            let removeIndx = fields.receipt.indexOf(fields.receipt.find(elm => elm.id === this.state.removeElm.id))
            fields.receipt.splice(removeIndx, 1)
            if (!this.state.testMode) {
                fields.receipt = fields.receipt.map(elm => {
                    return {
                        amount: elm.amount,
                        isRaw: isRawDropdownToCheckbox(elm.isRaw.id),
                        componentId: elm.componentId
                    }
                })
                let res = await this.props.onMutate({
                    mutation: SAVE_PRODUCT,
                    variables: fields
                }, errors.unpredictable, [{ query: GET_PRODUCT, variables: { id: this.props.match.params.id } }])
                for (let elm of res.queryResponse.product.receipt) {
                    elm.isRaw = isRawCheckboxToDropdown(elm.isRaw, elm.isMain)
                    swipeIds(elm)
                }
                this.setState({ product: res.queryResponse.product })
            } else this.setState({ product: fields })
        }
    }

    onDateChange = async (e) => {
        this.setState({ selectedDate: e.target.value })
        const date = moment(date).format()
        let res = await this.props.onQuery([{ query: GET_PRODUCT, variables: { id: this.props.match.params.id, date } }], false)
        if (res && res.product) {
            for (let elm of res.product.receipt) {
                elm.isRaw = isRawCheckboxToDropdown(elm.isRaw)
                swipeIds(elm)
            }
            this.setState({ product: res.product })
        }
        this.setState({ loading: false })
    }

    onCalculate = async () => {
        const [main, additional] = this.props.copy(this.state.product).receipt.reduce(([p, f], e) => ((e.isMain || !e.isRaw.id) ? [[...p, e], f] : [p, [...f, e]]), [[], []])
        let fields = {
            input: {
                groupId: this.state.group.id,
                percentSalaryCharges: this.state.percentSalaryCharges,
                percentRetail: this.state.percentRetail,
                mainComponents: main.map(elm => {
                    if (elm.isRaw.id) return {
                        amount: elm.amount,
                        price: elm.price,
                        currencyId: elm.currency.id
                    }
                    return {
                        amount: elm.amount,
                        price: elm.price,
                        currencyId: 1
                    }
                }),
            }
        }
        if (additional.length > 0) fields.input.additionalComponents = additional.map(elm => {
            return {
                amount: elm.amount,
                price: elm.price,
                currencyId: 1
            }
        })
        let res = await this.props.onQuery([{
            query: GET_COST,
            variables: fields
        }], errors.invalidData)
        for (let key of Object.keys(res.cost)) {
            try {
                res.cost[key] = res.cost[key].toFixed(2)
            } catch (err) {
                delete res.cost[key]
            }
        }

        res.cost.packages =  this.props.copy(this.state.product.packages)
        res.receipt = this.props.copy(this.state.product).receipt
        // res.percentSalaryCharges= this.state.percentSalaryCharges,
        res.cost.mainAmount = main.length > 0 ? main.reduce((accumulator, currentValue) => accumulator + currentValue.amount, 0) : 0
        res.cost.additionalAmount = additional.length > 0 ? additional.reduce((accumulator, currentValue) => accumulator.amount + currentValue.amount) : 0
        res.cost.percentProfit = this.state.group.percentProfit
        res.cost.percentRetail = this.state.percentRetail
        return res.cost
    }

    toggleTestMode = async (e) => {
        this.setState({ testMode: e.target.checked })
        if (!e.target.checked) {
            // this.setState({ loading: true })
            let res = await this.props.onQuery([{ query: GET_PRODUCT, variables: { id: this.props.match.params.id } }], false)
            if (res && res.product) {
                for (let elm of res.product.receipt) {
                    elm.isRaw = isRawCheckboxToDropdown(elm.isRaw, elm.isMain)
                    swipeIds(elm)
                }
                this.setState({ product: res.product })
            }
            // this.setState({ loading: false })
        }
    }

    render() {
        if (this.state.loading) return <Loading />
        if (!this.state.product) return <ErrorLogger error={errors.dataRetrieve} />
        return (
            <div style={styles.container}>
                <div style={styles.testModeContainer}>
                    <div style={this.state.testMode ? { ...styles.testModeLabel, color: '#f50057' } : styles.testModeLabel}>Тестовий режим</div>
                    <Switch checked={this.state.testMode} onChange={this.toggleTestMode} />
                </div>
                <div style={styles.backButton}><BackButton /></div>
                <TextField
                    id='date'
                    label='Дата'
                    type='date'
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={this.onDateChange}
                    value={this.state.selectedDate}
                />
                <CustomModal
                    open={this.state.openModal}
                    handleClose={this.onModalClose}
                    keys={heads}
                    values={this.state.modalValues}
                    onDropdownChange={(key, value) => {
                        if (key === 'isRaw') {
                            if (value) this.getRaw()
                            else this.getProducts()
                        }
                    }}
                />
                <CustomTable
                    heads={displayHeads}
                    data={this.state.product.receipt}
                    onAdd={this.onAddModalOpen}
                    onEdit={this.onEditModalOpen}
                    onRemove={(element) => this.setState({ removeElm: element, confirmRemoveOpen: true })}
                    tableName={this.state.title}
                />
                <div style={styles.textFieldsContainer}>
                    <div style={styles.textFieldsContainerJustifier}>
                        <div style={styles.textFieldLabel}>Нарахування на зарплату (%)</div>
                        <TextField
                            style={styles.textField}
                            variant='outlined'
                            type='number'
                            onChange={(e) => this.setState({ percentSalaryCharges: e.target.value })}
                            value={this.state.percentSalaryCharges}
                        />
                    </div>
                    <div style={styles.textFieldsContainerJustifier}>
                        <div style={styles.textFieldLabel}>Роздріб (%)</div>
                        <TextField
                            style={styles.textField}
                            variant='outlined'
                            type='number'
                            onChange={(e) => this.setState({ percentRetail: e.target.value })}
                            value={this.state.percentRetail}
                        />
                    </div>
                </div>
                <CostDataDialog onCalculate={this.onCalculate} />
                <Confirm
                    open={this.state.confirmRemoveOpen}
                    title={'Ви впевнені, що бажаєте видалити даний рецепт?'}
                    details={'Назва рецепту: ' + this.state.removeElm.name}
                    onResult={this.onRemove}
                />
            </div>
        )
    }
}

const styles = {
    textFieldsContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        flexDirection: 'column'
    },
    textFieldsContainerJustifier: {
        display: 'flex',
        flexDirection: 'column'
    },
    textFieldLabel: {
        textAlign: 'left',
        margin: 5
    },
    textField: {
        width: '250px'
    },
    testModeContainer: {
        position: 'absolute',
        right: 10,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    testModeLabel: {
        marginRight: 10,
        fontWeight: 'bold',
        color: 'grey'
    },
    container: {
        width: '100%'
    },
    backButton: {
        textAlign: 'left'
    }
}

export default withImmutable(withGraphQL(withAlert(withRouter(Product))))
