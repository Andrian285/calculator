import React, { Component } from 'react'
import CustomTable from '../components/CustomTable'
import config from '../utils/config'
import CustomModal from '../components/CustomModal'
import { withAlert } from 'react-alert'
import { withGraphQL, withImmutable } from '../hoc'
import { GET_PACKAGES, SAVE_PACKAGE, REMOVE_PACKAGE, GET_RAW, GET_PRODUCTS } from '../graphql'
import ErrorLogger from '../components/ErrorLogger'
import Confirm from '../components/Confirm'
import Loading from '../components/Loading'

const errors = config.errors

const displayHeads = [
    {
        label: 'Назва',
        keys: [{ prop: 'name' }],
    },
    {
        label: 'Вага, міра',
        keys: [{ prop: 'weight' }, { prop: 'measure' }],
        separator: ','
    },
    {
        label: 'Тара',
        keys: [{ prop: 'container', dropdown: true, keyName: 'name' }],
    },
    {
        label: 'Ярлик',
        keys: [{ prop: 'label', dropdown: true, keyName: 'name' }],
    },
    {
        label: 'Етикетка',
        keys: [{ prop: 'sticker', dropdown: true, keyName: 'name' }],
    },
    {
        label: 'Втрати (%)',
        keys: [{ prop: 'percentLoss' }]
    },
]

const heads = [
    { label: 'Назва', key: 'name' },
    { label: 'Вага', key: 'weight', numeric: true },
    { label: 'Міра', key: 'measure' },
    { label: 'Тара', key: 'containerId', dropdown: true },
    { label: 'Ярлик', key: 'labelId', dropdown: true },
    { label: 'Етикетка', key: 'stickerId', dropdown: true },
    { label: 'Втрати (%)', key: 'percentLoss', numeric: true },
]

const additionalOpts = {
    tableName: 'Фасовки'
}

class Raw extends Component {
    state = {
        openModal: false,
        modalValues: {},
        loading: true,
        confirmRemoveOpen: false,
        removeElm: {},
        idIncrementor: null
    }

    async componentDidMount() {
        await this.props.onQuery([{ query: GET_PACKAGES }, { query: GET_RAW }, { query: GET_PRODUCTS }])
        this.setState({ loading: false })
    }

    formatDropdowns(modalValues) {
        if (this.props.fetchedData.raws.length === 0 && this.props.fetchedData.products.length === 0) return
        let { raws, products } = this.props.copy(this.props.fetchedData)

        let incrementor = 0
        for (let elm of raws) incrementor += elm.id
        for (let elm of products) elm.id = elm.id + incrementor
        this.setState({ idIncrementor: incrementor })

        let items = this.props.copy([...raws, ...products])
        modalValues.containerId = {
            current: modalValues.container ?
                (modalValues.container.__typename === 'Raw' ? modalValues.container.id : modalValues.container.id + incrementor) : undefined,
            items
        }
        modalValues.containerRaw = modalValues.container ? modalValues.container.__typename === 'Raw' : null
        modalValues.labelId = {
            current: modalValues.label ?
                (modalValues.label.__typename === 'Raw' ? modalValues.label.id : modalValues.label.id + incrementor) : undefined,
            items
        }
        modalValues.labelRaw = modalValues.label ? modalValues.label.__typename === 'Raw' : null
        modalValues.stickerId = {
            current: modalValues.sticker ?
                (modalValues.sticker.__typename === 'Raw' ? modalValues.sticker.id : modalValues.sticker.id + incrementor) : undefined,
            items
        }
        modalValues.stickerRaw = modalValues.sticker ? modalValues.sticker.__typename === 'Raw' : null
        console.log(modalValues)
    }

    onEditModalOpen = (e, row) => {
        e.stopPropagation()
        let modalValues = this.props.copy(row)
        this.formatDropdowns(modalValues)
        this.setState({ modalValues, openModal: true })
    }

    onAddModalOpen = () => {
        let modalValues = {}
        this.formatDropdowns(modalValues)
        this.setState({ modalValues, openModal: true })
    }

    render() {
        if (this.state.loading) return <Loading />
        if (!this.props.fetchedData || !this.props.fetchedData.packages) return <ErrorLogger error='Щось пішло не так :(' />
        return (
            <div>
                <CustomModal
                    open={this.state.openModal}
                    handleClose={(data) => {
                        this.setState({ openModal: false, modalValues: {} })

                        if (data) {
                            if (data.containerId === undefined || data.containerId === -1) {
                                data.containerId = null
                                data.containerRaw = null
                            } else {
                                let containerRaw = this.props.fetchedData.raws.find(elm => elm.id === data.containerId)
                                if (!containerRaw) data.containerId -= this.state.idIncrementor
                            }
                            if (data.labelId === undefined || data.labelId === -1) {
                                data.labelId = null
                                data.labelRaw = null
                            } else {
                                let labelRaw = this.props.fetchedData.raws.find(elm => elm.id === data.labelId)
                                if (!labelRaw) data.labelId -= this.state.idIncrementor
                            }
                            if (data.stickerId === undefined || data.stickerId === -1) {
                                data.stickerId = null
                                data.stickerRaw = null
                            } else {
                                let stickerRaw = this.props.fetchedData.raws.find(elm => elm.id === data.stickerId)
                                if (!stickerRaw) data.stickerId -= this.state.idIncrementor
                            }

                            if (data.container) delete data.container
                            if (data.label) delete data.label
                            if (data.sticker) delete data.sticker
                            this.props.onMutate({
                                mutation: SAVE_PACKAGE,
                                variables: data
                            }, errors.invalidData, [{ query: GET_PACKAGES }])
                        }
                    }}
                    keys={heads}
                    values={this.state.modalValues}
                    onDropdownChange={(key, value) => {
                        let exists = false, isRaw = null, modalValues = this.state.modalValues
                        if (value !== -1) {
                            exists = this.props.fetchedData.raws.find(elm => elm.id === value)
                            isRaw = false
                        }
                        if (exists) isRaw = true
                        switch (key) {
                            case 'containerId':
                                modalValues.containerRaw = isRaw
                                break
                            case 'labelId':
                                modalValues.labelRaw = isRaw
                                break
                            case 'stickerId':
                                modalValues.stickerRaw = isRaw
                                break
                        }
                    }}
                />
                <CustomTable
                    heads={displayHeads}
                    data={this.props.fetchedData.packages}
                    onAdd={this.onAddModalOpen}
                    onEdit={this.onEditModalOpen}
                    onRemove={(element) => this.setState({ removeElm: element, confirmRemoveOpen: true })}
                    {...additionalOpts}
                />
                <Confirm
                    open={this.state.confirmRemoveOpen}
                    title={'Ви впевнені, що бажаєте видалити дану фасовку?'}
                    details={'Назва фасовки: ' + this.state.removeElm.name}
                    onResult={(statement) => {
                        this.setState({ confirmRemoveOpen: false })
                        if (statement) this.props.onMutate({
                            mutation: REMOVE_PACKAGE,
                            variables: { id: this.state.removeElm.id }
                        }, errors.unpredictable, [{ query: GET_PACKAGES }])
                    }}
                />
            </div>
        )
    }
}

export default withImmutable(withGraphQL(withAlert(Raw)))
