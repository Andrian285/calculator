import React, { Component } from 'react'
import CustomTable from '../components/CustomTable'
import CustomModal from '../components/CustomModal'
import { withAlert } from 'react-alert'
import { GET_GROUPS, SAVE_GROUP, REMOVE_GROUP } from '../graphql'
import Confirm from '../components/Confirm'
import ErrorLogger from '../components/ErrorLogger'
import config from '../utils/config'
import { withGraphQL, withImmutable } from '../hoc'
import Loading from '../components/Loading'

const errors = config.errors

const displayHeads = [
    {
        label: 'Назва',
        keys: [{ prop: 'name' }]
    },
    {
        label: 'Основна заробітна плата (за 1 т)',
        keys: [{ prop: 'baseSalary' }],
        numeric: true
    },
    {
        label: 'Додаткова заробітна плата (%)',
        keys: [{ prop: 'percentAddSalary' }],
        numeric: true
    },
    {
        label: 'Виробничі витрати (%)',
        keys: [{ prop: 'percentProdCosts' }],
        numeric: true
    },
    {
        label: 'Адміністративні витрати (%)',
        keys: [{ prop: 'percentAdminCosts' }],
        numeric: true
    },
    {
        label: 'Рентабельність (%)',
        keys: [{ prop: 'percentProfit' }],
        numeric: true
    },
    {
        label: 'Порядковий номер в прайсі',
        keys: [{ prop: 'priceNumber' }],
        numeric: true
    },
]

const heads = [
    { label: 'Назва', key: 'name' },
    { label: 'Основна заробітна плата (за 1 т)', key: 'baseSalary', numeric: true },
    { label: 'Додаткова заробітна плата (%)', key: 'percentAddSalary', numeric: true },
    { label: 'Виробничі витрати (%)', key: 'percentProdCosts', numeric: true },
    { label: 'Адміністративні витрати (%)', key: 'percentAdminCosts', numeric: true },
    { label: 'Рентабельність (%)', key: 'percentProfit', numeric: true },
    { label: 'Порядковий номер в прайсі', key: 'priceNumber', numeric: true },
]

const additionalOpts = {
    redirects: true,
    tableName: 'Групи продукції'
}

class ProductGroups extends Component {
    state = {
        openModal: false,
        modalValues: {},
        confirmRemoveOpen: false,
        removeElm: {},
        loading: true
    }

    async componentDidMount() {
        await this.props.onQuery([{ query: GET_GROUPS }])
        this.setState({ loading: false })
    }

    onEditModalOpen = (e, row) => {
        let modalValues = this.props.copy(row)
        this.setState({ modalValues, openModal: true })
    }

    render() {
        if (this.state.loading) return <Loading />
        if (!this.props.fetchedData || !this.props.fetchedData.groups) return <ErrorLogger error='Щось пішло не так :(' />
        return (
            <div>
                <CustomModal
                    open={this.state.openModal}
                    handleClose={(data) => {
                        this.setState({ openModal: false, modalValues: {} })
                        if (data) this.props.onMutate({
                            mutation: SAVE_GROUP,
                            variables: data
                        }, errors.invalidData, [{ query: GET_GROUPS }])
                    }}
                    keys={heads}
                    values={this.state.modalValues}
                />
                <CustomTable
                    heads={displayHeads}
                    data={this.props.fetchedData.groups}
                    onAdd={() => this.setState({ openModal: true, modalValues: {} })}
                    onEdit={this.onEditModalOpen}
                    onRemove={(element) => this.setState({ removeElm: element, confirmRemoveOpen: true })}
                    {...additionalOpts}
                />
                <Confirm
                    open={this.state.confirmRemoveOpen}
                    title={'Ви впевнені, що бажаєте видалити дану групу продукції?'}
                    details={'Назва групи: ' + this.state.removeElm.name}
                    onResult={(statement) => {
                        this.setState({ confirmRemoveOpen: false })
                        if (statement) this.props.onMutate({
                            mutation: REMOVE_GROUP,
                            variables: { id: this.state.removeElm.id }
                        }, errors.unpredictable, [{ query: GET_GROUPS }])
                    }}
                />
            </div>
        )
    }
}

export default withImmutable(withGraphQL(withAlert(ProductGroups)))
