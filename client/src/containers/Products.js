import React from 'react'
import { withRouter } from 'react-router-dom'
import CustomTable from '../components/CustomTable'
import config from '../utils/config'
import CustomModal from '../components/CustomModal'
import { withAlert } from 'react-alert'
import BackButton from '../components/BackButton'
import { GET_GROUP, SAVE_PRODUCT, REMOVE_PRODUCT, GET_PACKAGES } from '../graphql'
import { withGraphQL, withImmutable } from '../hoc'
import Loading from '../components/Loading'
import ErrorLogger from '../components/ErrorLogger'
import Confirm from '../components/Confirm'
import MultiSelector from '../components/MultiSelector'

const errors = config.errors

const displayHeads = [
    {
        label: 'Назва',
        keys: [{ prop: 'name' }],
    },
    {
        label: 'Входить в прайс',
        keys: [{ prop: 'priceInclude', checkbox: true }],
    },
    {
        label: 'Код',
        keys: [{ prop: 'code' }],
        numeric: true
    },
    {
        label: 'Код для експорту',
        keys: [{ prop: 'expCode' }],
        numeric: true
    },
    {
        label: 'Фасовки',
        keys: [{ prop: 'packages', multiselect: true, keyName: 'name' }],
        numeric: true
    },
]

const heads = [
    { label: 'Назва', key: 'name' },
    { label: 'Входить в прайс', key: 'priceInclude', checkbox: true },
    { label: 'Код', key: 'code', numeric: true },
    { label: 'Код для експорту', key: 'expCode', numeric: true },
]

const additionalOpts = {
    redirects: true
}

class Products extends React.Component {
    state = {
        groups: null,
        loading: true,
        title: '',
        openModal: false,
        confirmRemoveOpen: false,
        removeElm: {},
        modalValues: {}
    }

    async componentDidMount() {
        let res = await this.props.onQuery([{ query: GET_GROUP, variables: { id: this.props.match.params.id } }, { query: GET_PACKAGES }])
        if (res) this.setState({ loading: false, title: res.group.name })
    }

    onEditModalOpen = (e, row) => {
        e.stopPropagation()
        let modalValues = this.props.copy(row)
        modalValues.packages = modalValues.packages.map(elm => elm.id)
        this.setState({ modalValues, openModal: true })
    }

    onAddModalOpen = () => {
        this.setState({ modalValues: {}, openModal: true })
    }

    render() {
        if (this.state.loading) return <Loading />
        if (!this.props.fetchedData || !this.props.fetchedData.group || !this.props.fetchedData.group.products)
            return <ErrorLogger error={errors.dataRetrieve} />
        return (
            <div style={styles.container}>
                <div style={styles.backButton}><BackButton /></div>
                <CustomModal
                    open={this.state.openModal}
                    handleClose={(data) => {
                        this.setState({ openModal: false, modalValues: {} })
                        if (data) this.props.onMutate({
                            mutation: SAVE_PRODUCT,
                            variables: Object.assign({ groupId: parseInt(this.props.match.params.id) }, data)
                        }, errors.invalidData, [{ query: GET_GROUP, variables: { id: this.props.match.params.id } }])
                    }}
                    keys={heads}
                    values={this.state.modalValues}
                    customComponents={
                        <div style={{ margin: 10 }}>
                            <MultiSelector overrideStrings={{
                                selectSomeItems: 'Оберіть фасовки...',
                                selectAll: 'Обрати всі',
                                titleLabel: 'Обрано фасовок: '
                            }} data={this.props.fetchedData.packages.map(elm => ({ label: elm.name, value: elm.id }))}
                                onSelectedChanged={(selectedList) => {
                                    let modalValues = this.state.modalValues
                                    modalValues.packages = selectedList
                                    this.setState({ modalValues })
                                }}
                            selected={this.state.modalValues.packages} />
                        </div>
                    }
                />
                <CustomTable
                    heads={displayHeads}
                    data={this.props.fetchedData.group.products}
                    onAdd={this.onAddModalOpen}
                    onEdit={this.onEditModalOpen}
                    onRemove={(element) => this.setState({ removeElm: element, confirmRemoveOpen: true })}
                    tableName={this.state.title}
                    {...additionalOpts}
                />
                <Confirm
                    open={this.state.confirmRemoveOpen}
                    title={'Ви впевнені, що бажаєте видалити даний продукт?'}
                    details={'Назва продукту: ' + this.state.removeElm.name}
                    onResult={(statement) => {
                        this.setState({ confirmRemoveOpen: false })
                        if (statement) this.props.onMutate({
                            mutation: REMOVE_PRODUCT,
                            variables: { id: this.state.removeElm.id }
                        }, errors.unpredictable, [{ query: GET_GROUP, variables: { id: this.props.match.params.id } }])
                    }}
                />
            </div>
        )
    }
}

const styles = {
    container: {
        width: '100%'
    },
    backButton: {
        textAlign: 'left'
    }
}

export default withImmutable(withGraphQL(withAlert(withRouter(Products))))
