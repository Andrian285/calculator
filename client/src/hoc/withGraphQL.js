import React from 'react'
import config from '../utils/config'
import { withAlert } from 'react-alert'
import ErrorLogger from '../components/ErrorLogger'
import { generateQuery, generateQueryWithVariables } from '../graphql'

const errors = config.errors

function withRequests(Child) {
    return class extends React.Component {
        query = async (data) => {
            if (!data.query) throw new Error('Missing required parameter: query')
            let r = await fetch('/graphql', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify(data)
            })
            let result = await r.json()
            if (result.errors) throw new Error(result.errors)
            return result
        }

        mutate = async (data) => {
            if (!data.mutation || !data.variables) throw new Error('Missing required parameter: mutation')
            let formatted = {
                query: data.mutation,
                variables: data.variables
            }
            let result = await this.query(formatted)
            return result
        }

        render() {
            return (
                <Child query={this.query} mutate={this.mutate} {...this.props} />
            )
        }
    }
}

export default function withGrapQL(Child) {
    return withRequests(withAlert(class extends React.Component {
        state = {
            data: {},
            error: null
        }

        onQuery = async (data, save = true) => {
            if (!data) throw new Error('Missing required parameter: data/query')
            try {
                const [withVariables, simple] = data.reduce(([p, f], e) => (e.variables ? [[...p, e], f] : [p, [...f, e]]), [[], []])
                let result = {}
                if (simple.length > 0) {
                    let response = await this.props.query({ query: generateQuery(simple.map(elm => elm.query)) })
                    result = response.data
                    if (save) this.setState({ data: { ...this.state.data, ...result } })
                }
                if (withVariables.length > 0) {
                    for (let elm of withVariables) {
                        let response = null
                        response = await this.props.query({ query: generateQueryWithVariables(elm.query), variables: elm.variables })
                        result = { ...result, ...response.data }
                    }
                    if (save) this.setState({ data: { ...this.state.data, ...result } })
                }
                return JSON.parse(JSON.stringify(result))
            } catch (err) {
                console.log(err)
                this.setState({ error: errors.dataRetrieve })
            }
        }

        onMutate = async (data, error, refetchQueries) => {
            if (!data.mutation || !data.variables)
                throw new Error('Missing required parameter: mutation/variables')
            try {
                let res = {}
                res.mutationResponse = await this.props.mutate({
                    mutation: data.mutation,
                    variables: data.variables
                })
                if (refetchQueries) {
                    res.queryResponse = await this.onQuery(refetchQueries)
                }
                return res
            } catch (err) {
                console.log(err)
                this.props.alert.error(error)
            }
        }

        render() {
            if (this.state.error) return <ErrorLogger error={this.state.error} />
            return (
                <Child onQuery={this.onQuery} onMutate={this.onMutate} fetchedData={this.state.data} {...this.props} />
            )
        }
    }))
}
