import withGraphQL from './withGraphQL'
import withImmutable from './withImmutable'

export {
    withGraphQL,
    withImmutable
}