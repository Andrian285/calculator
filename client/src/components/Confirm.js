
import React from 'react'
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'

export default (props) => (
    <div>
        <Dialog
            open={props.open}
            onClose={() => props.onResult(false)}
            aria-labelledby='alert-dialog-title'
            aria-describedby='alert-dialog-description'
        >
            <DialogTitle id='alert-dialog-title'>{props.title}</DialogTitle>
            <DialogContent>
                <DialogContentText id='alert-dialog-description'>
                    {props.details}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.onResult(false)} color='primary'>
                    Ні
                </Button>
                <Button onClick={() => props.onResult(true)} color='primary' autoFocus>
                    Так
                </Button>
            </DialogActions>
        </Dialog>
    </div>
)
