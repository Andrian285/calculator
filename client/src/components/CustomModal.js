import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import config from '../utils/config'
import Modal from '@material-ui/core/Modal'
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import { withAlert } from 'react-alert'

const ComponentGenerator = (props) => {
    if (props.checkbox) return (
        <FormControlLabel
            control={
                <Checkbox checked={props.value ? props.value : false} onChange={e => props.onChange(e.target.checked)} />
            }
            label={props.label}
        />
    )
    if (props.dropdown) return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div style={{ textAlign: 'left', marginTop: 5 }}>{props.label}</div>
            <Select
                value={props.value.current !== undefined ? props.value.current : -1}
                onChange={e => props.onDropdownChange(e.target.value)}
                style={styles.dropdown}
            >
                {(props.value.current === undefined || props.value.current === -1) && <MenuItem value={-1}>Нічого</MenuItem>}
                {props.value.items.map(item =>
                    <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
                )}
            </Select>
        </div>
    )
    return (
        <TextField
            variant='outlined'
            type={props.numeric ? 'number' : 'text'}
            label={props.label}
            value={props.value ? props.value : ''}
            onChange={e => props.onChange(e.target.value)}
            style={{ marginTop: 10 }}
        />
    )
}

class CustomModal extends Component {
    state = {
        values: {}
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.values && !this.props.open && nextProps.open) this.setState({ values: nextProps.values })
        else if (!nextProps.values && !this.props.open && nextProps.open) this.setState({ values: {} })
        return true
    }

    onChange(key, value) {
        let values = this.state.values
        values[key] = value
        this.setState({ values })
    }

    onDropdownChange(key, value) {
        let values = this.state.values
        values[key].current = value
        this.setState({ values })
        if (this.props.onDropdownChange) this.props.onDropdownChange(key, value)
    }

    onClose() {
        let values = JSON.parse(JSON.stringify(this.state.values))
        for (let key of this.props.keys) {
            if (!values[key.key] && key.checkbox) values[key.key] = false
            if (key.dropdown) values[key.key] = values[key.key].current
            if (!key.checkbox && !values[key.key]) {
                if (values[key.key] === undefined && key.dropdown) continue
                this.props.alert.error('Всі поля повинні бути заповненими!')
                return null
            }
        }
        for (let key of Object.keys(values)) {
            let keyProps = this.props.keys.find(local_key => local_key.key === key)
            if (keyProps && keyProps.numeric) values[key] = parseFloat(values[key])
        }
        this.props.handleClose(values)
    }

    render() {
        return (
            <Modal
                open={this.props.open}
                onClose={() => this.props.handleClose()}
                style={styles.container}
            >
                <Paper style={styles.paper}>
                    {this.props.keys && this.props.keys.map(keyProps =>
                        <ComponentGenerator
                            value={this.state.values[keyProps.key]}
                            {...keyProps}
                            onChange={value => this.onChange(keyProps.key, value)}
                            onDropdownChange={value => this.onDropdownChange(keyProps.key, value)}
                        />
                    )}
                    {this.props.customComponents}
                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Button
                            variant='contained'
                            style={styles.saveButton}
                            onClick={() => this.props.handleClose()}
                        >
                            Скасувати
                        </Button>
                        <Button
                            variant='contained'
                            style={styles.saveButton}
                            onClick={() => this.onClose()}
                        >
                            Зберегти
                        </Button>
                    </div>
                </Paper>
            </Modal>
        )
    }
}

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    paper: {
        paddingLeft: 20,
        paddingRight: 20,
        outline: 'none',
        textAlign: 'center',
        display: 'flex',
        flexDirection: 'column',
        minWidth: '300px'
    },
    saveButton: {
        backgroundColor: config.primary_color,
        color: 'white',
        boxShadow: 'none',
        marginBottom: 10,
        marginTop: 10
    },
    dropdown: {
        marginTop: 10,
        marginBottom: 10
    }
}

export default withAlert(CustomModal)
