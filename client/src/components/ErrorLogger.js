import React from 'react'

export default class ErrorLogger extends React.Component {
    render() {
        return (
            <div style={styles.errorContainer}>
                <h1>{this.props.error}</h1>
            </div>
        )
    }
}

const styles = {
    errorContainer: {
        paddingLeft: 10, 
        paddingRight: 10
    }
}