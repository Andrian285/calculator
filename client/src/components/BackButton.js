
import React from 'react'
import { Icon, IconButton } from '@material-ui/core'
import { withRouter } from 'react-router-dom'

const BackButton = (props) => (
    <div style={{ marginLeft: 10, marginTop: 10, textAlign: 'left' }}>
        <IconButton onClick={() => props.history.goBack()}><Icon>arrow_back</Icon></IconButton>
    </div>
)

export default withRouter(BackButton)