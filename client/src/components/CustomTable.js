import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import Input from '@material-ui/core/Input'
import Checkbox from '@material-ui/core/Checkbox'
import config from '../utils/config'

const ComponentGenerator = (props) => {
    if (props.checkbox) return <Checkbox checked={props.value} style={{ padding: 0 }} />
    if (!props.value) return <div />
    if (props.dropdown) return props.value[props.keyName]
    if (props.multiselect) return props.value.map(elm => elm[props.keyName]).join(', ')
    if (props.numeric) return parseFloat(parseFloat(props.value).toFixed(2))
    return props.value
}

const ComponentsGenerator = (props) => {
    return (
        <div style={styles.element}>
            {props.keys.map((key, index) =>
                <React.Fragment key={index}>
                    <ComponentGenerator value={props.data[key.prop]} {...key} {...props} />
                    {index < props.keys.length - 1 && props.separator}
                    &nbsp;
                </React.Fragment>
            )}
        </div>
    )
}

class CustomTable extends Component {
    state = {
        hovered: null
    }

    handleHover(id) {
        this.setState({ hovered: id })
    }

    render() {
        const props = this.props
        return (
            <table style={styles.table}>
                <thead>
                    <tr>
                        {props.heads.map(head =>
                            <th key={head.label} style={styles.tableHead}>{head.label}</th>
                        )}
                    </tr>
                </thead>
                <tbody>
                    {props.data.map(row =>
                        <tr
                            style={props.redirects && this.state.hovered === row.id ? { cursor: 'pointer', backgroundColor: '#EAEAEA' } : {}}
                            key={row.id}
                            onMouseOver={() => this.handleHover(row.id)}
                            onMouseLeave={() => this.handleHover(null)}
                            onClick={() => {
                                if (props.redirects) {
                                    let currentPath = props.location.pathname
                                    if (currentPath[currentPath.length - 1] !== '/') currentPath += '/'
                                    props.history.push(currentPath + row.id)
                                }
                            }}
                        >
                            {props.heads.map((head, indx) =>
                                <td key={head.keys[0].prop} style={styles.tableCell}>
                                    <ComponentsGenerator
                                        data={row}
                                        {...head}
                                    />
                                </td>
                            )}
                            <td>
                                {this.state.hovered === row.id ?
                                    <div style={styles.buttonGroupContainer}>
                                        <Button
                                            variant='contained'
                                            style={styles.editButton}
                                            onClick={e => {
                                                e.stopPropagation()
                                                props.onEdit(e, row)
                                            }}
                                        >
                                            <i className='material-icons' style={styles.buttonGroupIcon}>edit</i>
                                        </Button>
                                        <Button
                                            variant='contained'
                                            style={styles.deleteButton}
                                            onClick={e => {
                                                e.stopPropagation()
                                                props.onRemove(row)
                                            }}
                                        >
                                            <i className='material-icons' style={styles.buttonGroupIcon}>delete</i>
                                        </Button>
                                    </div>
                                    :
                                    <div style={{ width: '150px' }} />
                                }
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        )
    }
}

class TableContainer extends Component {
    render() {
        let props = this.props
        return (
            <div style={styles.container}>
                <div style={styles.containerPadding}>
                    <div style={styles.headerContainer}>
                        {/* <div style={styles.searchContainer}>
                            <i className='material-icons' style={styles.searchIcon}>search</i>
                            <Input disableUnderline={true} placeholder='Search...' style={{ width: '100%' }} />
                        </div> */}
                        <Button
                            variant='contained'
                            style={styles.addButton}
                            onClick={() => props.onAdd()}
                        >
                            Додати
                        </Button>
                        {props.tableName && <p style={styles.headerTitle}>{props.tableName}</p>}
                    </div>
                    {!props.data || props.data.length === 0 ?
                        <div style={styles.emptyTableMessage}>{config.emptyTableMessage}</div>
                        :
                        <div style={styles.tableContainer}><CustomTable {...props} /></div>
                    }
                </div>
                {/* <Button
                    variant='contained'
                    style={{ backgroundColor: config.primary_color, color: 'white', boxShadow: 'none' }}
                >
                    Завантажити ще
                // </Button> */}
            </div>
        )
    }
}

const styles = {
    tableContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
    table: {
        borderCollapse: 'collapse'
    },
    tableHead: {
        color: 'grey',
        fontWeight: '500',
        padding: 'calc((100vw - 250px) / 180)',
    },
    tableCell: {
        borderTop: '1px solid #ddd',
        paddingTop: 10,
        paddingBottom: 10
    },
    container: {
        marginBottom: 10,
        maxWidth: 'calc(100vw - 250px)',
        display: 'flex',
        justifyContent: 'center'
    },
    containerPadding: {
        padding: 20,
        paddingBottom: 0
    },
    headerContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 5,
        paddingTop: 5,
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: 'Fira Sans Extra Condensed',
        fontSize: '20px',
        width: 'auto',
        margin: 0,
        padding: 0,
        paddingLeft: 15,
        whiteSpace: 'nowrap',
        textAlign: 'center'
    },
    searchContainer: {
        borderRadius: 3,
        width: '100%',
        paddingLeft: 5,
        paddingRight: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: config.content_color,
        marginRight: 5
    },
    searchIcon: {
        marginRight: 10
    },
    addButton: {
        backgroundColor: config.primary_color,
        color: 'white',
        boxShadow: 'none'
    },
    loading: {
        marginTop: 30,
        marginBottom: 30,
        color: config.primary_color
    },
    buttonGroupIcon: {
        fontSize: '20px'
    },
    buttonGroupContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: '20px',
        width: '150px'
    },
    editButton: {
        backgroundColor: config.primary_color,
        color: 'white',
        boxShadow: 'none',
        borderRadius: '5px 0px 0px 5px',
        borderRight: '2px solid ' + config.primary_darker_color
    },
    deleteButton: {
        backgroundColor: config.primary_color,
        color: 'white',
        boxShadow: 'none',
        borderRadius: '0px 5px 5px 0px'
    },
    element: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    emptyTableMessage: {
        margin: 10,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    }
}

export default withRouter(TableContainer)