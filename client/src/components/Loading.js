import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import config from '../utils/config'

export default () => (
    <CircularProgress style={styles.loading} />
)

const styles = {
    loading: {
        marginTop: 30,
        marginBottom: 30,
        color: config.primary_color
    }
}