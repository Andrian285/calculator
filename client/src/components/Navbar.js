import React from 'react'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import { withRouter } from 'react-router-dom'
import config from '../utils/config'

const tabs = [
    {
        label: 'Групи продукції',
        link: '/product-groups'
    },
    {
        label: 'Сировина',
        link: '/raw'
    },
    {
        label: 'Фасовки',
        link: '/packages'
    },
]

function ListItemLink(props) {
    return <ListItem button component='a' {...props} />
}

class Navbar extends React.Component {
    render() {
        return (
            <div style={styles.container}>
                <p style={styles.title}>
                    Калькулятор собівартості
            </p>
                <Divider style={styles.divider} />
                <List component='nav'>
                    {tabs.map(tab =>
                        <ListItemLink href={tab.link} key={tab.link}>
                            <ListItemText disableTypography primary={
                                <Typography
                                    type='body2'
                                    style={styles.elementLabel}
                                >
                                    {tab.label}
                                </Typography>
                            }
                            />
                        </ListItemLink>
                    )}
                </List>
            </div>
        )
    }
}

const styles = {
    container: {
        backgroundColor: config.secondary_color,
        height: '100vh',
        width: '350px',
        textAlign: 'center',
        zIndex: 1000
    },
    title: {
        color: 'white',
        margin: 0,
        paddingTop: 20,
        fontSize: '25px',
        fontFamily: 'Fira Sans Extra Condensed',
        paddingBottom: 20,
        backgroundColor: config.primary_color
    },
    elementLabel: {
        color: '#EFF2F3',
        textAlign: 'center',
        fontSize: '15px'
    },
    divider: {
        backgroundColor: 'white'
    }
}

export default withRouter(Navbar)
