import React from 'react'
import MultiSelect from '@khanacademy/react-multi-select'
import Checkbox from '@material-ui/core/Checkbox'

class ItemRenderer extends React.Component {
    render() {
        const { checked, option, onClick } = this.props

        return <span>
            <span>
                {option.label}
            </span>
            <Checkbox
                checked={checked}
                tabIndex='-1'
                onChange={onClick}
                style={{ padding: 0, float: 'right' }}
            />
        </span>
    }
}

export default class MySelector extends React.Component {
    valueRenderer = (selected) => {
        return this.props.overrideStrings.titleLabel + selected.length
    }

    render() {
        return (
            <MultiSelect
                options={this.props.data}
                onSelectedChanged={this.props.onSelectedChanged}
                selected={this.props.selected || []}
                valueRenderer={this.valueRenderer}
                ItemRenderer={ItemRenderer}
                isLoading={false}
                disabled={false}
                disableSearch
                overrideStrings={this.props.overrideStrings}
            />
        )
    }
}