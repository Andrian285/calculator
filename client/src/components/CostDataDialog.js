import React, { Component } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Paper from '@material-ui/core/Paper'
import config from '../utils/config'
import { Divider } from '@material-ui/core'

const Label = (props) => (
    <div style={styles.dataLabel}>{props.data}</div>
)

const Data = (props) => (
    <div>{props.data}</div>
)

const DataContainer = (props) => (
    <Paper style={styles.paper}>
        {props.data.map(elm =>
            <div style={{ display: 'flex', flexDirection: 'row', marginTop: 5 }} key={elm.label}>
                <Label data={elm.label} />
                <Data data={elm.data} />
            </div>
        )}
    </Paper>
)

const roundUpToValue = (num, value) => {
    return Number((Math.ceil(num * (1 / value)) / value).toFixed(2))
}

export default class CostDataDialog extends Component {
    state = {
        open: false,
        costLoading: false,
        data: null
    }

    handleClickOpen = async () => {
        let data = await this.props.onCalculate()
        for (let key of Object.keys(data)) {
            if (!Array.isArray(data[key])) data[key] = Number(data[key])
        }
        this.setState({ data, open: true })
    }

    handleClose = () => {
        this.setState({ open: false })
    }

    render() {
        //FORMULAS FOR PACKAGES COST
        let rawCostForPackages = this.state.data ?
            this.state.data.salaryCost +
            this.state.data.prodCost +
            this.state.data.mainRawCost +
            this.state.data.additionalRawCost : 0
        
        let prodCostsForPkgs = [], packages = this.state.data ? this.state.data.packages : []
        for (let pkg of packages) prodCostsForPkgs.push({
            label: 'Виробнича собівартість без ПДВ за 1 кг по фасовці: ' + pkg.weight + ' ' + pkg.measure, 
            data: 30.55 + ' грн.'
        })

        let prodCostsForPkgsVAT = []
        for (let pkg of packages) prodCostsForPkgsVAT.push({
            label: 'Виробнича собівартість з ПДВ за 1 кг по фасовці: ' + pkg.weight + ' ' + pkg.measure, 
            data: parseFloat(((pkg.cost + rawCostForPackages) * 1.2 / 1000).toFixed(2)) + ' грн.'
        })

        let total = this.state.data ? this.state.data.adminCost + rawCostForPackages : 0

        let fullCostsForPkgs = []
        for (let pkg of packages) fullCostsForPkgs.push({
            label: 'Повна собівартість без ПДВ за 1 кг по фасовці: ' + pkg.weight + ' ' + pkg.measure, 
            data: parseFloat(((pkg.cost + total) / 1000).toFixed(2)) + ' грн.'
        })

        let fullCostsForPkgsVAT = []
        for (let pkg of packages) fullCostsForPkgsVAT.push({
            label: 'Повна собівартість без ПДВ за 1 кг по фасовці: ' + pkg.weight + ' ' + pkg.measure, 
            data: parseFloat(((pkg.cost + total) * 1.2 / 1000).toFixed(2)) + ' грн.'
        })

        

        let standartProdWithVAT = this.state.data ? roundUpToValue(total * (1 + this.state.data.percentProfit / 100.0), 0.06) : 0
        let retailPriceWithVAT = this.state.data ? roundUpToValue(standartProdWithVAT * (1 + this.state.data.percentRetail / 100.0), 0.06) : 0

        return (
            <div>
                <Button
                    variant='contained'
                    style={styles.button}
                    onClick={this.handleClickOpen}
                    color='secondary'
                >
                    {this.state.costLoading ?
                        <CircularProgress style={styles.loading} size={20} />
                        : 'Підрахувати собівартість'
                    }
                </Button>
                {this.state.data &&
                    <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby='alert-dialog-title'
                        aria-describedby='alert-dialog-description'
                    >
                        <DialogTitle id='alert-dialog-title'>{'Собівартість продукції'}</DialogTitle>
                        <DialogContent>
                            <DataContainer
                                data={[
                                    { label: 'Основна сировина: загальна кількість', data: parseFloat(this.state.data.mainAmount.toFixed(2)) + ' кг' },
                                    { label: 'Основна сировина: загальна сума', data: parseFloat(this.state.data.mainRawCost.toFixed(2)) + ' грн.' },
                                    { label: 'Додаткова сировина: загальна сума', data: parseFloat(this.state.data.additionalRawCost.toFixed(2)) + ' грн.' },
                                    { label: 'Сировинна собівартість за 1 кг', data: parseFloat(((this.state.data.mainRawCost + this.state.data.additionalRawCost) / 1000.0).toFixed(2)) + ' грн.' },
                                    { label: 'Адміністративні витрати: загальна сума', data: parseFloat(this.state.data.adminCost.toFixed(2)) + ' грн.' },
                                    { label: 'Адміністративні витрати за 1 кг', data: parseFloat((this.state.data.adminCost / 1000).toFixed(2)) + ' грн.' },
                                    { label: 'Заробітна плата: загальна сума', data: parseFloat(this.state.data.salaryCost.toFixed(2)) + ' грн.' },
                                    { label: 'Заробітна плата за 1 кг', data: parseFloat((this.state.data.salaryCost / 1000).toFixed(2)) + ' грн.' },
                                    { label: 'Виробничі витрати: загальна сума', data: parseFloat(this.state.data.prodCost.toFixed(2)) + ' грн.' },
                                    { label: 'Виробничі витрати за 1 кг без ПДВ', data: parseFloat((this.state.data.prodCost / 1000).toFixed(2)) + ' грн.' },
                                    { label: 'Виробничі витрати за 1 кг з ПДВ', data: parseFloat((this.state.data.prodCost / 1000 * 1.2).toFixed(2)) + ' грн.' },
                                    ...prodCostsForPkgs,
                                    ...prodCostsForPkgsVAT,
                                    ...fullCostsForPkgs,
                                    ...fullCostsForPkgsVAT
                                    //{ label: 'Ціна із стандартною рентабельністю з ПДВ', data: parseFloat(parseFloat(withStandartProdWithVAT).toFixed(2)) + ' грн.' }, //+
                                    //{ label: 'Роздрібна ціна з ПДВ', data: parseFloat(parseFloat(withStandartProdWithVAT + roundUpToValue(withStandartProdWithVAT), 0.06).toFixed(2)) + ' грн.' }, //+
                                ]}
                            // summary={{ label: 'Загальна собівартість', data: total + ' грн.' }}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color='primary'>
                                Закрити
                        </Button>
                        </DialogActions>
                    </Dialog>
                }
            </div>
        )
    }
}

const styles = {
    button: {
        color: 'white',
        boxShadow: 'none'
    },
    loading: {
        color: 'white'
    },
    paper: {
        padding: 10
    },
    dataElementContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    dataLabel: {
        fontWeight: 'bold',
        color: 'grey',
        marginRight: 15
    },
    footerContainer: {
        marginTop: 10,
        textAlign: 'center'
    },
    footerLabel: {
        fontWeight: 'bold',
        color: 'grey'
    }
}
