export default {
    primary_color: '#444A57',
    primary_darker_color: '#21242A',
    secondary_color: '#282C34',
    content_color: '#E4E5E5',
    emptyTableMessage: 'На даний момент таблиця пуста',
    errors: {
        dataRetrieve: 'Виникла помилка при спробі завантажити сторінку.',
        invalidData: 'Щось пішло не так! Можливо, ви вказали неправильні дані.',
        unpredictable: 'Щось пішло не так :('
    }
}