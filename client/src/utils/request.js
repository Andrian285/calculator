export default async function request(url, method, body, token, page) {
    if (!url || !method) throw new Error('Request argument error: required arguments missing!')

    let headers = { 'Content-Type': 'application/json', 'Accept': 'application/json' }
    if (token) headers.token = token
    if (page) headers.page = page

    let obj = {
        method: method.toLowerCase(),
        headers: headers,
    }

    if (method.toLowerCase() === 'post' || method.toLowerCase() === 'put') obj.body = JSON.stringify(body)

    let response = await fetch(url, obj)
    return response
}
