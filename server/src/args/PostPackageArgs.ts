export default class PostPackageArgs {
    id?: number
    name?: string
    weight?: number
    measure?: string
    percentLoss?: number
    containerId?: number
    containerRaw?: boolean
    labelId?: number
    labelRaw?: boolean
    stickerId?: number
    stickerRaw?: boolean
}