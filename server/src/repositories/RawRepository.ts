import { Service } from "typedi";
import { EntityManager, LessThanOrEqual, FindOneOptions } from "typeorm";
import { RawPrice, Raw } from "../entity";
import * as RawModel from "../model/Raw";

@Service()
export class RawRepository {

    constructor(private entityManager: EntityManager) {
    }

    async populate({ id, date }): Promise<RawModel.Raw> {
        let findOptions: FindOneOptions = {
            order: { "createdAt": "DESC" }, relations: ["raw"]
        };
        let relationCondition = { raw: this.entityManager.create(Raw, { id }) };
        findOptions.where = date
            ? { ...relationCondition, createdAt: LessThanOrEqual(date) }
            : { ...relationCondition };
        let rawPrice = await this.entityManager.findOne<RawPrice>(RawPrice, findOptions);
        let raw = rawPrice.raw;
        let rawModel: RawModel.Raw = {
            id: raw.id, name: raw.name, measure: raw.measure, isMain: raw.isMain,
            price: rawPrice.price, currencyId: rawPrice.currencyId,
            isTypeOf: RawModel.Raw.prototype.isTypeOf
        };
        return rawModel;
    }
}