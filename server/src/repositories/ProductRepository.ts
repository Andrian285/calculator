import { Service } from "typedi";
import { EntityManager, LessThanOrEqual, FindOneOptions, MoreThan, IsNull, FindManyOptions } from "typeorm";
import * as ProductModel from "../model/Product";
import * as RawModel from "../model/Raw";
import { Product } from "../entity/Product";
import { ProductComponent } from "../entity/ProductComponent";
import { RawRepository } from "./RawRepository";
import { CurrencyRepository } from "./CurrencyRepository";

@Service()
export class ProductRepository {

    constructor(private entityManager: EntityManager, private rawRepository: RawRepository, private currencyRepository: CurrencyRepository) {
    }

    async populate({ id, date }): Promise<ProductModel.Product> {
        let findProductOptions: FindOneOptions = {
            relations: ["group", "packages"]
        };
        let product = await this.entityManager.findOne<Product>(Product, id, findProductOptions);
        return { ...product, isTypeOf: ProductModel.Product.prototype.isTypeOf };
    }

    async receipt({ product, date }): Promise<{ raws, products }> {
        let dateFilter = date
            ? { createdAt: LessThanOrEqual(date), endedAt: MoreThan(date) }
            : { endedAt: IsNull() };

        let findComponentsOptions: FindManyOptions = {
            where: { ...dateFilter, product },
        }

        let components = await this.entityManager.find<ProductComponent>(ProductComponent, findComponentsOptions);

        let rawPromises = components.filter(c => c.isRaw).map(async r => {
            return {
                isTypeOf: RawModel.Raw.prototype.isTypeOf, componentId: r.id, isRaw: r.isRaw, amount: r.amount,
                ...await this.rawRepository.populate({ id: r.componentId, date })
            } as RawModel.Raw;
        });
        let productPromises = components.filter(c => !c.isRaw).map(async p => {
            return { componentId: p.id, isRaw: p.isRaw, amount: p.amount, ...await this.populate({ id: p.componentId, date }) } as ProductModel.Product;
        });

        let raws = await Promise.all(rawPromises);
        let products = await Promise.all(productPromises);
        return { raws, products };
    }

    async price({ product, date }): Promise<number> {
        let cost = 0;
        let receipt = await this.receipt({ product, date });
        let currencies = await this.currencyRepository.getAll({date});

        receipt.raws.forEach(r => {
            cost += r.amount * r.price * currencies.find(c => c.id === r.currencyId).rate;
        });

        receipt.products.forEach(async p => {
            cost += await this.price({ product: p, date });
            console.log(cost);
        })

        const percentSalaryCharges = 22;

        cost /= 1000;
        if (cost > 0) {
            let group = product.group;
            let salary = group.baseSalary * (1 + group.percentAddSalary / 100.0);
            let salaryCost = salary * (1 + percentSalaryCharges / 100.0) / 1000;
            let prodCost = salaryCost * (group.percentProdCosts / 100.0);
            return salaryCost + cost + prodCost;
        }
        return cost;
    }
}