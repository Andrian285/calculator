import { Service } from "typedi";
import { Currency } from "../model/Currency";
import axios from "axios";
import logger from "../logger";

@Service()
export class CurrencyRepository {
    private grivna = { id: 1, name: "Гривня", rate: 1, sign: "UAH" } as Currency;
    private bankUrl = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?"
    constructor() {
    }

    getGrivna(): Currency {
        return this.grivna;
    }

    async getAll({ date }): Promise<Currency[]> {
        try {
            let responses = await axios.all([
                axios.get(`${this.bankUrl}valcode=usd&json`),
                axios.get(`${this.bankUrl}valcode=eur&json`),
            ]);
            let currencies = responses.filter(res => res.data[0]).map(res => {
                let r = res.data[0];
                if (r) return { id: r.r030, name: r.txt, rate: r.rate, sign: r.cc } as Currency
            })
            currencies.push(this.grivna);
            return currencies;
        } catch (e) {
            logger.error(e);
            return [];
        }
    }

    async getById({ id, date }): Promise<Currency> {
        try {
            return (await this.getAll(date)).find(c => c.id === id);
        } catch (e) {
            logger.error(e);
            return this.grivna;
        }
    }
}