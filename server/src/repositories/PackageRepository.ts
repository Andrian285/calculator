import { Service } from "typedi";
import { EntityManager } from "typeorm";
import * as PackageModel from "../model/Package";
import { Package } from "../entity/Package";

@Service()
export class PackageRepository {

    constructor(private entityManager: EntityManager) {
    }

    async populate({ id }): Promise<PackageModel.Package> {
        return (await this.entityManager.findOne(Package, id)) as PackageModel.Package;
    }
}