import { Service } from "typedi";
import PostPackageArgs from "../args/PostPackageArgs";
import { EntityManager } from "typeorm";
import { Raw } from "../entity/Raw";
import { Product } from "../entity";
import { ArgsValidatorInterface } from "vesper";

@Service()
export default class PostPackageArgsValidator implements ArgsValidatorInterface<PostPackageArgs> {
    constructor(private entityManager?: EntityManager) {
    }

    async validate(args: PostPackageArgs) {
        if(args.containerId){
            let cont;
            if(args.containerRaw){
                cont = await this.entityManager.findOne(Raw, args.containerId); 
            } else {
                cont = await this.entityManager.findOne(Product, args.containerId);
            }
            if(!cont) throw new Error("Cannot add container with such ID");
        }

        if(args.labelId){
            let label;
            if(args.labelRaw){
                label = await this.entityManager.findOne(Raw, args.labelId); 
            } else {
                label = await this.entityManager.findOne(Product, args.labelId);
            }
            if(!label) throw new Error("Cannot add label with such ID");
        }

        if(args.stickerId){
            let sticker;
            if(args.stickerRaw){
                sticker = await this.entityManager.findOne(Raw, args.stickerId); 
            } else {
                sticker = await this.entityManager.findOne(Product, args.stickerId);
            }
            if(!sticker) throw new Error("Cannot add sticker with such ID");
        }
    }

}