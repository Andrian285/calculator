export class Product {
    id: number;
    name: string;
    code: number;
    expCode: number;
    priceInclude: boolean;
    group: object;
    receipt?: object[];
    packages?: object[];
    price?: number;
    amount?: number;
    isRaw?: boolean;
    componentId?: number;
    currency?: object;
    currencyId?: number;
    isTypeOf() { return "Product"; };
}