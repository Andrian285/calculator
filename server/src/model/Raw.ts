export class Raw {
    id: number;
    name: string;
    isMain: boolean;
    measure: string;
    price: number;
    currency?: object;
    currencyId?: number;
    componentId?: number;
    isTypeOf(): string { return "Raw"; };
}