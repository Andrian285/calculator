export class Currency {
    id: number;
    name: string;
    sign: string;
    rate: number;
}