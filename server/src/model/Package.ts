export class Package {
    id: number;
    weight: number;
    name: string;
    measure: string;
    container?: object;
    label?: object;
    sticker?: object;
    containerId: number;
    labelId: number;
    stickerId: number;
    containerRaw: boolean;
    labelRaw: boolean;
    stickerRaw: boolean;
    percentLoss: number;
}
