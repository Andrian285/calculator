import { Controller, Query } from "vesper";
import { EntityManager } from "typeorm";
import { ProductGroup } from "../entity/ProductGroup";
import { CurrencyRepository } from "../repositories/CurrencyRepository";

@Controller()
export class CostController {

    constructor(private entityManager: EntityManager, private currencyRepository: CurrencyRepository) {
    }

    @Query()
    async cost({ input }) {
        let group = await this.entityManager.findOne<ProductGroup>(ProductGroup, input.groupId);
        let currencies = await this.currencyRepository.getAll({ date: null });
        let additionalRawCost = 0;
        let mainRawCost = 0;
        input.mainComponents.forEach(c => {
            mainRawCost += c.amount * c.price * currencies.find(curr => curr.id === c.currencyId).rate;
        });

        if (input.additionalComponents) {
            input.additionalComponents.forEach(c => {
                additionalRawCost += c.amount * c.price * currencies.find(curr => curr.id === c.currencyId).rate;
            });
        }

        let salary = group.baseSalary * (1 + group.percentAddSalary / 100.0);
        let salaryCost = salary * (1 + input.percentSalaryCharges / 100.0);
        let prodCost = salaryCost * (group.percentProdCosts / 100.0);
        let adminCost = salaryCost * (group.percentAdminCosts / 100.0);
        return { mainRawCost, additionalRawCost, salaryCost, prodCost, adminCost };
    }

}