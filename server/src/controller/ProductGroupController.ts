import { Controller, Query, Mutation } from "vesper";
import { EntityManager } from "typeorm";
import { ProductGroup } from "../entity/ProductGroup";
import logger from '../logger';

@Controller()
export class ProductGroupController {

    constructor(private entityManager: EntityManager) {
    }

    @Query()
    groups() {
        return this.entityManager.find(ProductGroup);
    }

    @Query()
    group({ id }) {
        return this.entityManager.findOne(ProductGroup, id);
    }

    @Mutation()
    async groupSave(args) {
        try {
            const group = this.entityManager.create(ProductGroup, args);
            await this.entityManager.save(ProductGroup, group);
            return true;
        } catch (e) {
            logger.error(e);
            throw(e);
        }
    }

    @Mutation()
    async groupDelete({ id }) {
        const status = await this.entityManager.remove(ProductGroup, { id });
        console.log(status);
        return status.id;
    }

}