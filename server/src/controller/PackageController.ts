import { Controller, Query, Mutation, ArgsValidator } from "vesper";
import { EntityManager } from "typeorm";
import logger from '../logger';
import { Package, Raw } from "../entity";
import { PackageRepository } from "../repositories/PackageRepository";
import PostPackageArgsValidator  from "../validators/PostPackageArgsValidator";
import PostPackageArgs from "../args/PostPackageArgs";

@Controller()
export class PackageController {

    constructor(private entityManager: EntityManager, private packageRepository: PackageRepository) {
    }

    @Query()
    async packages() {
        let packages = await this.entityManager.find(Package);
        return packages.map(p => this.packageRepository.populate({ id: p.id }));
    }


    @Query()
    package({ id }) {
        return this.packageRepository.populate({ id });
    }

    @Mutation()
    @ArgsValidator(PostPackageArgsValidator)
    async packageSave(args: PostPackageArgs) {
        try {
            let pack: Package;
            if (args.id) {
                pack = await this.entityManager.findOne(Package, args.id);
            }
            pack = this.entityManager.create(Package, args);
            console.log(pack);
            await this.entityManager.save(Package, pack);
            return true;
        } catch (e) {
            logger.error(e);
            throw e;
        }
    }

    @Mutation()
    async packageDelete({ id }) {
        const status = await this.entityManager.remove(Package, { id });
        console.log(status);
        return status.id;
    }

}