import { Controller, Query, Mutation } from "vesper";
import { EntityManager } from "typeorm";
import { Raw } from "../entity/Raw";
import { Currency, RawPrice } from "../entity";
import { RawRepository } from "../repositories/RawRepository";
import logger from '../logger';

@Controller()
export class RawController {

    constructor(private entityManager: EntityManager, private rawRepository: RawRepository) {
    }

    @Query()
    async raws({ date }) {
        let raws = await this.entityManager.find(Raw, { select: ["id"] });
        return Promise.all(raws.map(r => this.rawRepository.populate({ id: r.id, date })));
    }

    @Query()
    raw({ id, date }) {
        return this.rawRepository.populate({ id, date });
    }

    @Mutation()
    async rawSave(args) {
        try {
            let raw = this.entityManager.create(Raw, args);
            raw = await this.entityManager.save(Raw, raw);

            let rawPrice: RawPrice = await this.entityManager.findOne(RawPrice, {
                where: { rawId: raw.id },
                order: { "createdAt": "DESC" }
            });
            if (args.isVAT && args.price) {
                args.price = args.price / 1.2;
            }
            if (!rawPrice ||
                (args.price && args.price != rawPrice.price ||
                    args.currencyId && args.currencyId !== rawPrice.currencyId)) {
                {

                    const rawPriceArgs = { ...rawPrice, ...args, raw };
                    delete rawPriceArgs.id;
                    let newRawPrice = this.entityManager.create(RawPrice, rawPriceArgs);
                    await this.entityManager.save(newRawPrice);
                }
            }
            return true;
        } catch (e) {
            logger.error(e);
            throw (e);
        }
    }

    @Mutation()
    async rawDelete({ id }) {
        const status = await this.entityManager.remove(Raw, { id });
        console.log(status);
        return status.id != undefined;
    }
}