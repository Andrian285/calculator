import { Controller, Query, Mutation } from "vesper";
import { EntityManager, IsNull } from "typeorm";
import { Product } from "../entity/Product";
import { ProductGroup } from "../entity/ProductGroup";
import { ProductComponent, Package } from "../entity";
import { ProductRepository } from "../repositories/ProductRepository";
import logger from "../logger";

@Controller()
export class ProductController {

    constructor(private entityManager: EntityManager, private productRepository: ProductRepository) {
    }

    @Query()
    async products({ date }) {
        let products = await this.entityManager.find(Product, { select: ["id"] });
        return Promise.all(products.map(p => this.productRepository.populate({ id: p.id, date })));
    }

    @Query()
    product({ id, date }) {
        return this.productRepository.populate({ id, date });
    }

    @Mutation()
    async productSave(args) {
        try {
            console.log(args);
            const groupId = args.groupId;
            let group: ProductGroup;
            if (groupId) {
                group = await this.entityManager.findOne(ProductGroup, { id: groupId });
            }
            let product = this.entityManager.create(Product, args);
            product.group = group;
            product = await this.entityManager.save(Product, product);

            let updatedComponents: ProductComponent[] = args.receipt;

            if (updatedComponents) {
                let uniqueSize = new Set(updatedComponents.map(c => { return { id: c.componentId, isRaw: c.isRaw } })).size;
                if (uniqueSize !== updatedComponents.length) {
                    throw new Error("Some components are duplicating!")
                }
                if (updatedComponents.findIndex(c => !c.isRaw && c.componentId === product.id) > -1) {
                    throw new Error("Unable to add same product in it receipt!")
                }
                let currentComponents = await this.entityManager.find(ProductComponent, {
                    where: { product, endedAt: IsNull() }
                });
                if (currentComponents) {
                    currentComponents.forEach(async c => {
                        let index = updatedComponents.findIndex(
                            i => i.componentId === c.componentId
                                && i.amount === c.amount
                                && i.isRaw === c.isRaw);

                        let toSave = [];
                        if (index > -1) updatedComponents.splice(index, 1);
                        else {
                            toSave.push(this.entityManager.create(ProductComponent, { ...c, endedAt: new Date() }));
                        }
                        await this.entityManager.save(toSave);
                    });
                }
                await this.entityManager.save(updatedComponents.map(c => {
                    c.product = product;
                    return this.entityManager.create(ProductComponent, c);
                }));
            };
            let updatedPackages: number[] = args.packages;
            if (updatedPackages) {
                let currentPackages = product.packages;
                currentPackages.forEach(p => {
                    let index = updatedPackages.findIndex(u => u === p.id);
                    if (index > -1) updatedPackages.slice(index, 1);
                });
                product.packages = await this.entityManager.findByIds(Package, updatedPackages);
                await this.entityManager.save(Product, product);
            }
            return true;
        } catch (e) {
            logger.error(e);
            throw e;
        }
    }

    @Mutation()
    async productDelete({ id }) {
        try {
            const status = await this.entityManager.remove(Product, { id });
            console.log(status);
            return status.id;
        } catch (e) {
            logger.error(e);
            return false;
        }
    }
}