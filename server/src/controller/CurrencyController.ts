import { Controller, Query } from "vesper";
import { EntityManager } from "typeorm";
import { CurrencyRepository } from "../repositories/CurrencyRepository";

@Controller()
export class CurrencyController {
    constructor(private entityManager: EntityManager, private currencycRepository: CurrencyRepository) {
    }

    @Query()
    currencies() {
        return this.currencycRepository.getAll({date: null});
    }

    @Query()
    currency({ id }) {
        return this.currencycRepository.getById({id, date: null});
    }
}