import { Resolver, ResolverInterface, Resolve } from "vesper";
import { EntityManager } from "typeorm";
import { Raw } from "../model/Raw";
import { CurrencyRepository } from "../repositories/CurrencyRepository";

@Resolver(Raw)
export class RawResolver implements ResolverInterface<Raw> {

    constructor(private entityManager: EntityManager, private currencyRepository: CurrencyRepository) {
    }

    @Resolve()
    async currency(raws: Raw[]) {
        const currencies = await this.currencyRepository.getAll({date: null});
        return raws.map(r => currencies.find(c => c.id === r.currencyId));
    }
}