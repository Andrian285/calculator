import { Resolver, ResolverInterface, Resolve } from "vesper";
import { EntityManager } from "typeorm";
import { Package } from "../model/Package";
import { RawRepository } from "../repositories/RawRepository";
import { ProductRepository } from "../repositories/ProductRepository";
import { Currency } from "../model/Currency";
import { CurrencyRepository } from "../repositories/CurrencyRepository";

@Resolver(Package)
export class PackageResolver implements ResolverInterface<Package> {

    constructor(private entityManager: EntityManager, private rawRepository: RawRepository, private productRepository: ProductRepository, private currencyRepository: CurrencyRepository) {
    }

    @Resolve()
    container(packages: Package[], args) {
        return Promise.all(packages.map(async p => p.containerRaw == null ? null :
            (p.containerRaw
                ? await this.rawRepository.populate({ id: p.containerId, date: null })
                : await this.productRepository.populate({ id: p.containerId, date: null }))));
    }

    @Resolve()
    label(packages: Package[], args) {
        return Promise.all(packages.map(async p => p.labelRaw == null ? null :
            (p.labelRaw
                ? await this.rawRepository.populate({ id: p.labelId, date: null })
                : await this.productRepository.populate({ id: p.labelId, date: null }))));
    }

    @Resolve()
    sticker(packages: Package[], args) {
        return Promise.all(packages.map(async p => p.stickerRaw == null ? null :
            (p.stickerRaw
                ? await this.rawRepository.populate({ id: p.stickerId, date: null })
                : await this.productRepository.populate({ id: p.stickerId, date: null }))));
    }

    @Resolve()
    cost(packages: Package[], args) {
        return Promise.all(packages.map(async p => {
            let currencies = await this.currencyRepository.getAll({ date: null });
            let cost = 1000 / p.weight * (1 + p.percentLoss / 100.0);
            let sumComp = 0.0;

            let sticker = p.stickerRaw == null ? null :
                (p.stickerRaw
                    ? await this.rawRepository.populate({ id: p.stickerId, date: null })
                    : await this.productRepository.populate({ id: p.stickerId, date: null }));

            if (sticker) {
                if (sticker.isTypeOf() == "Product") {
                    sumComp += await this.productRepository.price({ product: sticker, date: null });
                } else {
                    sumComp += sticker.price * (currencies.find(c => c.id === sticker.currencyId)).rate;
                }
            }

            let label = p.labelRaw == null ? null :
                (p.labelRaw
                    ? await this.rawRepository.populate({ id: p.labelId, date: null })
                    : await this.productRepository.populate({ id: p.labelId, date: null }));

            if (label) {
                if (label.isTypeOf() == "Product") {
                    sumComp += await this.productRepository.price({ product: label, date: null });
                } else {
                    sumComp += label.price * (currencies.find(c => c.id === label.currencyId)).rate;
                }
            }

            let container = p.containerRaw == null ? null :
                (p.containerRaw
                    ? await this.rawRepository.populate({ id: p.containerId, date: null })
                    : await this.productRepository.populate({ id: p.containerId, date: null }));

            if (container) {
                if (container.isTypeOf() == "Product") {
                    sumComp += await this.productRepository.price({ product: container, date: null });
                } else {
                    sumComp += container.price * (currencies.find(c => c.id === container.currencyId)).rate;
                }
            }

            return cost * sumComp;
        }))
    }
};

