import { Resolver, ResolverInterface, Resolve } from "vesper";
import { EntityManager } from "typeorm";
import { Product } from "../model/Product";
import { ProductRepository } from "../repositories/ProductRepository";
import { CurrencyRepository } from "../repositories/CurrencyRepository";

@Resolver(Product)
export class ProductResolver implements ResolverInterface<Product> {

    constructor(private entityManager: EntityManager, private productRepository: ProductRepository, private currencyRepository: CurrencyRepository) {
    }

    @Resolve()
    price(products: Product[], args) {
        return Promise.all(products.map(async p => await this.productRepository.price({ product: p, date: args.date })));
    }

    @Resolve()
    receipt(products: Product[], args) {
        return Promise.all(products.map(async p => {
            let { raws, products } = await this.productRepository.receipt({ product: p, date: args.date });
            return [].concat(raws, products);
        }));
    }

    @Resolve()
    currency(products: Product[], args) {
        return products.map(p => this.currencyRepository.getGrivna());
    }
}