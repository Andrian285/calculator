import "reflect-metadata";
import { bootstrap } from "vesper";
import { GraphQLDate, GraphQLDateTime, GraphQLTime } from 'graphql-iso-date';
import { Currency, Product, ProductGroup, Raw, RawPrice, ProductComponent, Package } from './entity/index';
import { CurrencyController, ProductController, ProductGroupController, RawController, CostController, PackageController } from './controller/index';
import { CorsOptions } from "cors";
import { ProductResolver } from "./resolvers/ProductResolver";
import express = require("express");
import { RawResolver } from "./resolvers/RawResolver";
import { PackageResolver } from "./resolvers/PackageResolver";

require('dotenv').config();

const app = express();
const path = require("path")

// app.use(express.static(path.join(__dirname + '/../../client/', 'build')));
// app.get('/*', (res: express.Response) => res.sendFile(path.join(__dirname + '/../../client/build/index.html')));

bootstrap({
    expressApp: app,
    cors: { allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept"] } as CorsOptions,
    port: Number(process.env.PORT) || 8080,
    controllers: [
        CurrencyController,
        ProductController,
        ProductGroupController,
        RawController,
        CostController,
        PackageController
    ],
    entities: [
        Currency,
        Product,
        ProductGroup,
        Raw,
        RawPrice,
        ProductComponent,
        Package
    ],
    customResolvers: {
        Date: GraphQLDate,
        Time: GraphQLTime,
        DateTime: GraphQLDateTime,
        Component: {
            __resolveType: type => type.isTypeOf()
        },
        PackageComponent: {
            __resolveType: type => type.isTypeOf()
        }
    },
    
    resolvers: [ProductResolver, RawResolver, PackageResolver],
    schemas: [
        __dirname + "/schema/**/*.graphql"
    ]
}).then(() => {
    console.log("Your app is up and running on http://localhost:8080. " +
        "You can use playground in development mode on http://localhost:8080/playground");

}).catch(error => {
    console.error(error.stack ? error.stack : error);
});
