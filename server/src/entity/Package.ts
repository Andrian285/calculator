import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from "typeorm";
import { Product } from ".";

@Entity()
export class Package {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "float" })
    weight: number;

    @Column()
    measure: string;

    @Column()
    name: string;

    @Column({ nullable: true })
    containerId: number;

    @Column({ nullable: true })
    containerRaw: boolean;

    @Column({ nullable: true })
    labelId: number;

    @Column({ nullable: true })
    labelRaw: boolean;

    @Column({ nullable: true })
    stickerId: number;

    @Column({ nullable: true })
    stickerRaw: boolean;

    @Column()
    percentLoss: number;

    @ManyToMany(() => Product, product => product.packages)
    products: Product[]
}
