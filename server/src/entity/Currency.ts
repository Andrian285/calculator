import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import { RawPrice } from "./RawPrice";

@Entity()
export class Currency {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    name: string;

    @Column({nullable: false})
    sign: string;
/*
    @OneToMany(type => RawPrice, rawPrice => rawPrice.currency)
    rawPrices: RawPrice[]*/
}
