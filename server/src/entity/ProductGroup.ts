import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Product } from './Product';

@Entity()
export class ProductGroup {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true, nullable: false })
    name: string;

    @Column({ nullable: false })
    baseSalary: number;

    @Column({ nullable: false })
    percentAddSalary: number;

    @Column({ nullable: false })
    percentProfit: number;

    @Column({ nullable: false })
    percentProdCosts: number;

    @Column({ nullable: false })
    percentAdminCosts: number;

    @Column({ nullable: false, unique: true })
    priceNumber: number;

    @OneToMany(type => Product, product => product.group)
    products: Product[];

}
