import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, PrimaryColumn, JoinColumn, CreateDateColumn } from "typeorm";
import { ProductGroup } from "./ProductGroup";
import { Product, Raw } from ".";

@Entity()
export class ProductComponent {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    componentId: number

    @ManyToOne(() => Product, product => product.components, { onDelete: "CASCADE" })
    product: Product;

    @CreateDateColumn()
    createdAt

    @Column({ type: "timestamp", nullable: true })
    endedAt: Date;

    @Column({ nullable: false })
    amount: number;

    @Column({ nullable: false })
    isRaw: boolean;
}