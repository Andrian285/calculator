import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, Timestamp, CreateDateColumn } from "typeorm";
import { Raw } from "./Raw";
import { Currency } from ".";

@Entity()
export class RawPrice {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false, type: "float" })
    price: number;

    @CreateDateColumn()
    createdAt

    @Column()
    currencyId: number

    @ManyToOne(() => Raw, raw => raw.prices, { onDelete: "CASCADE" })
    raw: Raw
}
