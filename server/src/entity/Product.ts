import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable } from "typeorm";
import { ProductGroup } from "./ProductGroup";
import { ProductComponent } from "./ProductComponent";
import { Package } from "./Package";

@Entity()
export class Product {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true, nullable: false })
    name: string;

    @Column({ unique: true, nullable: false })
    code: number;

    @Column({ unique: true, nullable: false })
    expCode: number;

    @Column({ nullable: false })
    priceInclude: boolean;

    @ManyToOne(() => ProductGroup, group => group.products, { onDelete: "CASCADE" })
    group: ProductGroup

    @OneToMany(() => ProductComponent, productComponent => productComponent.product)
    components: ProductComponent[]

    @ManyToMany(() => Package, pack => pack.products)
    @JoinTable()
    packages: Package[]
}
