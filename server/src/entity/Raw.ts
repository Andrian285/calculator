import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { RawPrice } from "./RawPrice";

@Entity()
export class Raw {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true, nullable: false })
    name: string;

    @Column({ nullable: false })
    measure: string;

    @Column({ nullable: false })
    isMain: boolean;

    @OneToMany(type => RawPrice, price => price.raw)
    prices: RawPrice[];
}
